/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   
 *   Balasko, Jeno
 *   Baranyi, Botond
 *   Delic, Adam
 *   Forstner, Matyas
 *   Lovassy, Arpad
 *   Raduly, Csaba
 *   Szabo, Janos Zoltan – initial implementation
 *   Szalai, Gabor
 *
 ******************************************************************************/
/*
 * Common header file for the Base Library of the TTCN-3 Test Executor
 *
 * This file is included from the output files of the TTCN-3 compiler.
 * You should include this file instead of the individual header files of this
 * directory in your C++ modules that are linked to the executable test suite.
 *
 * Do not modify this file.
 * Code originally authored by Janos Zoltan Szabo
 *
 */

#ifndef TTCN3_HH
#define TTCN3_HH

#include <functional>

#ifdef MEMORY_DEBUG
// this is needed so the global debug_new_counter object is destroyed after
// the global objects in the generated C++ files are destroyed
#include "dbgnew.hh"
#else
// include memory.h directly, if it's not included through dbgnew.hh
#include "memory.h"
#endif

#include "version.h"
#include <cstring>

#include "JSON.hh"
#include "Vector.hh"
#include "Basetype.hh"
#include "Integer.hh"
#include "Float.hh"
#include "Boolean.hh"
#include "ASN_Null.hh"
#include "Objid.hh"
#include "Bitstring.hh"
#include "Octetstring.hh"
#include "ASN_Any.hh"
#include "Charstring.hh"
#include "Universal_charstring.hh"
#include "Struct_of.hh"
#include "TTCN3_Map.hh"
#include "Optional.hh"
#include "ASN_CharacterString.hh"
#include "ASN_External.hh"
#include "ASN_EmbeddedPDV.hh"
#include "Addfunc.hh"
#include "Encdec.hh"
#include "BER.hh"
#include "OER.hh"
#include "PER.hh"
#include "Error.hh"

#ifndef TITAN_ASN1ENC
#include "Verdicttype.hh"
#include "Template.hh"
#include "Component.hh"
#include "Hexstring.hh"
#include "Array.hh"
#include "Timer.hh"
#include "Port.hh"
#include "Logger.hh"

#ifdef TITAN_RUNTIME_2
#include "RT2/TitanLoggerApiSimple.hh"
#include "RT2/PreGenRecordOf.hh"
#else
#include "RT1/TitanLoggerApiSimple.hh"
#include "RT1/PreGenRecordOf.hh"
#endif

#include "Module_list.hh"
#include "Parameters.h"
#include "Snapshot.hh"
#include "Default.hh"
#include "Runtime.hh"
#include "RAW.hh"
#include "TEXT.hh"
#include "XER.hh"
#include "XmlReader.hh"
#include "Profiler.hh"
#include "Debugger.hh"
#include "OOP.hh"
#endif // #ifndef TITAN_ASN1ENC

#endif
