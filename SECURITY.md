# Security Policy

This project implements the Eclipse Foundation Security Policy

* https://www.eclipse.org/security

## Supported Versions

These versions of Eclipse Titan are currently being supported with security
updates.

| Version | Released   | Supported          | 
| ------- | ---------- | ------------------ | 
| 11.0.0  | 2024-11-14 | :white_check_mark: | 
| 10.1.2  | 2024-07-09 | :white_check_mark: | 
| 10.1.1  | 2024-06-05 | :white_check_mark: | 
| 10.1.0  | 2024-04-25 | :white_check_mark: | 
| 10.0.0  | 2023-11-15 | :white_check_mark: | 
| 9.0.0   | 2023-05-17 | :x:                | 
| 8.3.0   | 2023-01-17 | :x:                | 
| 8.2.0   | 2022-05-09 | :x:                | 
| < 8.1.0 | 2021-12-15 | :x:                | 

## Reporting a Vulnerability

Please report vulnerabilities to the Eclipse Foundation Security Team at
security@eclipse.org
