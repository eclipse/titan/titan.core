/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/
module LazyFuzzy_SW { //^In TTCN-3 module//

function f1(@lazy integer l, @fuzzy integer f) {}

function f2(inout integer io, integer i) return integer {
  io := io + i;
  return io;
}

function f3(out integer o, integer i) return integer {
  o := i;
  return i;
}


control { //^In control part//
  var integer v1 := 0;
  var integer v2 := 0;
  f1(l := f2(v1, 2) - 1, f := f3(v2, 3) + 1); //^In function instance// //^In actual parameter list of function// //^In parameter//2 //The actual parameter for a @lazy or @fuzzy formal parameter contains a reference to function `@LazyFuzzy_SW.f2', which has `out' or `inout' parameters. This is forbidden by the TTCN-3 standard, but is allowed in TITAN for backward compatibility// //The actual parameter for a @lazy or @fuzzy formal parameter contains a reference to function `@LazyFuzzy_SW.f3', which has `out' or `inout' parameters. This is forbidden by the TTCN-3 standard, but is allowed in TITAN for backward compatibility//
}

}
