/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/
module ReuseEnumName_SW { //^In TTCN-3 module//2

type enumerated MyEnumType { enumX, enumY } //Reused enumerated value is here//2
type enumerated MyEnumType2 { enumY, enumZ } //Reused enumerated value is here//

const integer enumX := 3; //Definition `enumX' reuses the identifier of an enumerated value declared in this module. This is forbidden by the TTCN-3 standard, but is allowed in TITAN for backward compatibility//

function control() { //^In control part//
  var MyEnumType enumY := enumX;  //Definition `enumY' reuses the identifier of an enumerated value declared in this module. This is forbidden by the TTCN-3 standard, but is allowed in TITAN for backward compatibility//
  var MyEnumType enumW := enumY;
  var MyEnumType enumZ; //Definition `enumZ' reuses the identifier of an enumerated value declared in this module. This is forbidden by the TTCN-3 standard, but is allowed in TITAN for backward compatibility//
  action(enumW);
}

}
