/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/

#include "PerAST.hh"
#include <cstddef>

PerAST::PerAST()
: extendable(FALSE), field_order(NULL), nof_ext_adds(0), ext_add_indexes(NULL),
  enum_values(NULL), enum_codes(NULL)
{
}

PerAST::~PerAST()
{
  delete[] field_order;
  delete[] ext_add_indexes;
  delete[] enum_values;
  delete[] enum_codes;
}
