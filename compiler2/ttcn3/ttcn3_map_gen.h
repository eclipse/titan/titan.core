/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond – initial implementation
 *
 ******************************************************************************/
#ifndef TTCN3_MAP_GEN_H
#define TTCN3_MAP_GEN_H

#include "../datatypes.h"
#include "compiler.h"

#ifdef __cplusplus
extern "C" {
#endif

void defTtcn3MapClass(const ttcn3_map_def *sdef, output_struct *output);
void defTtcn3MapTemplate(const ttcn3_map_def *sdef, output_struct *output);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* MAP_GEN_H */
