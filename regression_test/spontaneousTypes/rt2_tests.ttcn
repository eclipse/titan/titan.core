/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/

// tests for spontaneously created types, i.e. user defined types created for a single
// constant, template, variable, parameter, etc.
// (this module contains tests for runtime 2 only)
module rt2_tests {

import from spontaneous_types all;

testcase tc_spontaneous_types_rt2() runs on CT {
  var Rec v_rec := { 2, omit };
  
  var record of Rec v_inout := { v_rec };
  var record of Rec v_inout_exp := { v_rec, v_rec };
  
  var set of enumerated { e1, e2 } v_out;
  var set of enumerated { e1, e2 } v_out_exp := { e1, e2 };
  
  var template set { integer i1, integer i2 } vt_inout := { i1 := 3, i2 := 5 };
  var template set { integer i1, integer i2 } vt_inout_exp := { i1 := 4, i2 := ? };
  
  var template set { union { boolean b, Rec r } u1, union { integer i, charstring cs } u2 } vt_out;
  var template set { union { boolean b, Rec r } u1, union { integer i, charstring cs } u2 } vt_out_exp :=
    { u1 := { b := true }, u2 := { i := (1..2) } };
  
  var record { Rec r, set of charstring socs } v_ret :=
    f2({ v_rec }, v_inout, v_out, { "a", "bb", "ccc" }, vt_inout, vt_out);
  var record { Rec r, set of charstring socs } v_ret_exp := { r := v_rec, socs := { "a", "bb", "ccc" } };
  
  if (v_inout != v_inout_exp) {
    setverdict(fail, "Invalid 'inout' value parameter. Expected: ", v_inout_exp, ", got: ", v_inout);
  }
  
  if (v_out != v_out_exp) {
    setverdict(fail, "Invalid 'out' value parameter. Expected: ", v_out_exp, ", got: ", v_out);
  }
  
  if (log2str(vt_inout) != log2str(vt_inout_exp)) {
    setverdict(fail, "Invalid 'inout' template parameter. Expected: ", vt_inout_exp, ", got: ", vt_inout);
  }
  
  if (log2str(vt_out) != log2str(vt_out_exp)) {
    setverdict(fail, "Invalid 'out' template parameter. Expected: ", vt_out_exp, ", got: ", vt_out);
  }
  
  if (v_ret != v_ret_exp) {
    setverdict(fail, "Invalid return value. Expected: ", v_ret_exp, ", got: ", v_ret);
  }
  setverdict(pass);
}

testcase tc_spontaneous_types_sig_rt2() runs on CT {
  connect(self:pt, self:pt);
  
  var Rec v_rec := { 31, "31" };
  
  var record { Rec r } v_in := { r := v_rec };
  var record of Rec v_inout := { v_rec, v_rec };
  var set of enumerated { e1, e2 } v_out := { e1, e1, e2 };
  var record { Rec r, set of charstring socs } v_ret := { r := v_rec, socs := { "this", "that" } };
  var record { integer num, charstring str } v_exc := { num := 21, str := "something" };
  
  timer tmr := 0.2;
  pt.call(Sig2: { v_in, v_inout, - }, nowait);
  tmr.start;
  alt {
    [] pt.getcall(Sig2: { v_in, v_inout, - }) { tmr.stop; }
    [] tmr.timeout { setverdict(fail, "getcall timed out"); }
  }
  
  pt.reply(Sig2: { -, v_inout, v_out } value v_ret);
  tmr.start;
  alt {
    [] pt.getreply(Sig2: { -, v_inout, v_out } value v_ret) { tmr.stop; }
    [] tmr.timeout { setverdict(fail, "getreply timed out"); }
  }
  
  pt.raise(Sig2, v_exc);
  tmr.start;
  alt {
    [] pt.catch(Sig2, v_exc) { tmr.stop; }
    [] tmr.timeout { setverdict(fail, "catch timed out"); }
  }
  setverdict(pass);
}

control {
  execute(tc_spontaneous_types_rt2());
  execute(tc_spontaneous_types_sig_rt2());
}

}
