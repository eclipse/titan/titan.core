/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/

// tests for spontaneously created types, i.e. user defined types created for a single
// constant, template, variable, parameter, etc.
module spontaneous_types {

type record Rec {
  integer num,
  charstring str optional
}

type union Type_c { integer i };
type enumerated Type_ce { one, two };
type set Type_t { enumerated { e1(1), e2(2), e3(3) } e, integer i };
type record of set { octetstring os, float f } Type_mp;
type set of set of integer Type_mpt;
type record Type_v { integer num, charstring str optional };
type record of integer Type_vt;

const union { integer i } c := { i := 1 };

external const enumerated { one, two } ce;

template set { enumerated { e1(1), e2(2), e3(3) } e, integer i } t := { i := ?, e := e2 };

modulepar record of set { octetstring os, float f } mp := { { '12'O, 1.0 }, { 'AB'O, -3.5 } };

modulepar template set of set of integer mpt := { ? length (1..3), ? length (2..5), ? };

function f1(in record of integer p_in,
            inout record of octetstring p_inout,
            out set of boolean p_out,
            in template set of charstring pt_in,
            inout template set of float pt_inout,
            out template set of bitstring pt_out)
            return record of integer
{
  p_inout[2] := 'FEED'O;
  p_out := { true, false, false };
  pt_inout[1] := ?;
  pt_out := ? length (6..30);
  return { lengthof(p_in), lengthof(pt_in) };
}

function f2(in record { Rec r } p_in,
            inout record of Rec p_inout,
            out set of enumerated { e1, e2 } p_out,
            in template set of charstring pt_in,
            inout template set { integer i1, integer i2 } pt_inout,
            out template set { union { boolean b, Rec r } u1, union { integer i, charstring cs } u2 } pt_out)
            return record { Rec r, set of charstring socs }
{
  p_inout[1] := p_inout[0];
  p_out[0] := e1;
  p_out[1] := e2;
  pt_inout.i1 := valueof(pt_inout.i1) + 1;
  pt_inout.i2 := ?;
  pt_out.u1.b := true;
  pt_out.u2.i := (1..2);
  return { r := p_in.r, socs := valueof(pt_in) };
}

signature Sig1(in record of integer p_in,
               inout record of charstring p_inout,
               out set of bitstring p_out)
               return record of boolean
               exception (record of octetstring); 

signature Sig2(in record { Rec r } p_in,
               inout record of Rec p_inout,
               out set of enumerated { e1, e2 } p_out)
               return record { Rec r, set of charstring socs }
               exception (record { integer num, charstring str });             
              
type port PT procedure {
  inout Sig1, Sig2
}
with { extension "internal" }

type component CT {
  port PT pt;
}

testcase tc_spontaneous_types() runs on CT {
  const Type_c c_exp := { i := 1 };
  if (log2str(c) != log2str(c_exp)) {
    setverdict(fail, "Invalid constant. Expected: ", c_exp, ", got: ", c);
  }
  const Type_ce ce_exp := two;
  if (log2str(ce) != log2str(ce_exp)) {
    setverdict(fail, "Invalid external constant. Expected: ", ce_exp, ", got: ", ce);
  }
  
  template Type_t t_exp := { i := ?, e := e2 };
  if (log2str(t) != log2str(t_exp)) {
    setverdict(fail, "Invalid template. Expected: ", t_exp, ", got: ", t);
  }
  
  var Type_mp mp_exp := { { '12'O, 1.0 }, { 'AB'O, -3.5 } };
  if (log2str(mp) != log2str(mp_exp)) {
    setverdict(fail, "Invalid module parameter. Expected: ", mp_exp, ", got: ", mp);
  }
  
  var template Type_mpt mpt_exp := { ? length (1..3), ? length (2..5), ? };
  if (log2str(mpt) != log2str(mpt_exp)) {
    setverdict(fail, "Invalid module parameter template. Expected: ", mpt_exp, ", got: ", mpt);
  }
  
  var record { integer num, charstring str optional } v := { 1, "a" };
  var Type_v v_exp := { 1, "a" };
  if (log2str(v) != log2str(v_exp)) {
    setverdict(fail, "Invalid variable. Expected: ", v_exp, ", got: ", v);
  }
  
  var template record of integer vt := { 1, 2, 3 };
  var template Type_vt vt_exp := { 1, 2, 3 };
  if (log2str(vt) != log2str(vt_exp)) {
    setverdict(fail, "Invalid template variable. Expected: ", vt_exp, ", got: ", vt);
  }
  
  var record of octetstring v_inout := { '12'O, '34'O, '56'O, '78'O };
  var record of octetstring v_inout_exp := { '12'O, '34'O, 'FEED'O, '78'O };
  
  var set of boolean v_out;
  var set of boolean v_out_exp := { false, true, false };
  
  var template set of float vt_inout := { 1.34, -5.0 };
  var template set of float vt_inout_exp := { 1.34, ? };
  
  var template set of bitstring vt_out;
  var template set of bitstring vt_out_exp := ? length (6..30);
  
  var record of integer v_ret := f1({ 2, 4, 6, 8 }, v_inout, v_out, { "a", "bb", "ccc" }, vt_inout, vt_out);
  var record of integer v_ret_exp := { 4, 3 };
  
  if (v_inout != v_inout_exp) {
    setverdict(fail, "Invalid 'inout' value parameter. Expected: ", v_inout_exp, ", got: ", v_inout);
  }
  
  if (v_out != v_out_exp) {
    setverdict(fail, "Invalid 'out' value parameter. Expected: ", v_out_exp, ", got: ", v_out);
  }
  
  if (log2str(vt_inout) != log2str(vt_inout_exp)) {
    setverdict(fail, "Invalid 'inout' template parameter. Expected: ", vt_inout_exp, ", got: ", vt_inout);
  }
  
  if (log2str(vt_out) != log2str(vt_out_exp)) {
    setverdict(fail, "Invalid 'out' template parameter. Expected: ", vt_out_exp, ", got: ", vt_out);
  }
  
  if (v_ret != v_ret_exp) {
    setverdict(fail, "Invalid return value. Expected: ", v_ret_exp, ", got: ", v_ret);
  }
  setverdict(pass);
}

testcase tc_spontaneous_types_sig() runs on CT {
  connect(self:pt, self:pt);
  
  var record of integer v_in := { 1, 5, 9 };
  var record of charstring v_inout := { "abc", "def" };
  var set of bitstring v_out := { '101'B, '110'B, '10001'B };
  var record of boolean v_ret := { true, true };
  var record of octetstring v_exc := { '1A'O, '2B'O, '3C'O };
  
  timer tmr := 0.2;
  pt.call(Sig1: { v_in, v_inout, - }, nowait);
  tmr.start;
  alt {
    [] pt.getcall(Sig1: { v_in, v_inout, - }) { tmr.stop; }
    [] tmr.timeout { setverdict(fail, "getcall timed out"); }
  }
  
  pt.reply(Sig1: { -, v_inout, v_out } value v_ret);
  tmr.start;
  alt {
    [] pt.getreply(Sig1: { -, v_inout, v_out } value v_ret) { tmr.stop; }
    [] tmr.timeout { setverdict(fail, "getreply timed out"); }
  }
  
  pt.raise(Sig1, v_exc);
  tmr.start;
  alt {
    [] pt.catch(Sig1, v_exc) { tmr.stop; }
    [] tmr.timeout { setverdict(fail, "catch timed out"); }
  }
  setverdict(pass);
}

control {
  execute(tc_spontaneous_types());
  execute(tc_spontaneous_types_sig());
}

}
