/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond
 *
 ******************************************************************************/
module any_type_param {

type component CT {}

type record Rec {
  integer num,
  charstring str
}

type record of integer IntList;

external function fx_value(any p_data := 0) return integer;

external function fx_template(template any p_data := "xyz") return integer;

external function fx_ref(inout any p_ref, out template any p_ret);

testcase tc_any_type_param_value() runs on CT {
  var integer ret := fx_value(4);
  var integer exp := 4;
  if (ret != exp) {
    setverdict(fail, "Test #1: expected ", exp, ", got ", ret);
  }
  ret := fx_value("abc");
  exp := 3;
  if (ret != exp) {
    setverdict(fail, "Test #2: expected ", exp, ", got ", ret);
  }
  ret := fx_value(Rec: { num := 6, str := "xy" });
  exp := 6;
  if (ret != exp) {
    setverdict(fail, "Test #3: expected ", exp, ", got ", ret);
  }
  ret := fx_value(IntList: { 1, 2 });
  exp := 2;
  if (ret != exp) {
    setverdict(fail, "Test #4: expected ", exp, ", got ", ret);
  }
  ret := fx_value(); // default value
  exp := 0;
  if (ret != exp) {
    setverdict(fail, "Test #5: expected ", exp, ", got ", ret);
  }
  setverdict(pass);
}

testcase tc_any_type_param_template() runs on CT {
  var integer ret := fx_template(4);
  var integer exp := 4;
  if (ret != exp) {
    setverdict(fail, "Test #1: expected ", exp, ", got ", ret);
  }
  ret := fx_template("abc");
  exp := 3;
  if (ret != exp) {
    setverdict(fail, "Test #2: expected ", exp, ", got ", ret);
  }
  ret := fx_template(Rec: { num := 6, str := "xy" });
  exp := 6;
  if (ret != exp) {
    setverdict(fail, "Test #3: expected ", exp, ", got ", ret);
  }
  ret := fx_template(IntList: { 1, 2 });
  exp := 2;
  if (ret != exp) {
    setverdict(fail, "Test #4: expected ", exp, ", got ", ret);
  }
  ret := fx_template(-); // default value
  exp := 3;
  if (ret != exp) {
    setverdict(fail, "Test #5: expected ", exp, ", got ", ret);
  }
  setverdict(pass);
}

testcase tc_any_type_param_ref() runs on CT {
  var integer p_int := 4;
  var template integer pt_int;
  fx_ref(p_int, pt_int);
  var integer p_int_exp := 5;
  var template integer pt_int_exp := 4;
  if (p_int != p_int_exp or log2str(pt_int) != log2str(pt_int_exp)) {
    setverdict(fail, "Test #1: expected ", p_int_exp, ", and ", pt_int_exp, "; got ", p_int, ", and ", pt_int);
  }
  var charstring p_char := "abc";
  var template charstring pt_char;
  fx_ref(p_char, pt_char);
  var charstring p_char_exp := "abcx";
  var template charstring pt_char_exp := "abc";
  if (p_char != p_char_exp or log2str(pt_char) != log2str(pt_char_exp)) {
    setverdict(fail, "Test #2: expected ", p_char_exp, ", and ", pt_char_exp, "; got ", p_char, ", and ", pt_char);
  }
  var Rec p_rec := { num := 2, str := "xy" };
  var template Rec pt_rec;
  fx_ref(p_rec, pt_rec);
  var Rec p_rec_exp := { num := 3, str := "xyx" };
  var template Rec pt_rec_exp := { num := 2, str := "xy" };
  if (p_rec != p_rec_exp or log2str(pt_rec) != log2str(pt_rec_exp)) {
    setverdict(fail, "Test #3: expected ", p_rec_exp, ", and ", pt_rec_exp, "; got ", p_rec, ", and ", pt_rec);
  }
  var IntList p_list := { 1, 2, 3 };
  var template IntList pt_list;
  fx_ref(p_list, pt_list);
  var IntList p_list_exp := { 2, 3, 4 };
  var template IntList pt_list_exp := { 1, 2, 3} ;
  if (p_list != p_list_exp or log2str(pt_list) != log2str(pt_list_exp)) {
    setverdict(fail, "Test #4: expected ", p_list_exp, ", and ", pt_list_exp, "; got ", p_list, ", and ", pt_list);
  }
  setverdict(pass);
}

control {
  execute(tc_any_type_param_value());
  execute(tc_any_type_param_template());
  execute(tc_any_type_param_ref());
}

}
