/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond – initial implementation
 *
 ******************************************************************************/
module ExternalTests {

import from Types all;
import from Tools all;

// unaligned
external function enc_ext(in External x) return bitstring with { extension "prototype(convert) encode(PER)" }

external function dec_ext(in bitstring x) return External with { extension "prototype(convert) decode(PER)" }

// aligned
external function enc_ext_a(in External x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }

external function dec_ext_a(in bitstring x) return External with { extension "prototype(convert) decode(PER:ALIGNED)" }

testcase tc_per_external_encode_unaligned() runs on CT {
  var External x1 := { { syntax := objid_val }, omit, '1234'O };
  var bitstring enc_exp := '1000000001100000000000000010000001101000000100001001000110100000'B;
  var bitstring enc := enc_ext(x1);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #1 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x2 := { { syntax := objid_val }, "abc", '1234'O };
  enc_exp := '101000000110000000000000001000000110000001101100001011000100110001101000000100001001000110100000'B;
  enc := enc_ext(x2);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #2 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x3 := { { presentation_context_id := 117 }, omit, '1234'O };
  enc_exp := '010000000010111010101000000100001001000110100000'B;
  enc := enc_ext(x3);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #3 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x4 := { { presentation_context_id := 117 }, "abc", '1234'O };
  enc_exp := '01100000001011101010000001101100001011000100110001101000000100001001000110100000'B;
  enc := enc_ext(x4);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #4 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x5 := { { context_negotiation := { 117, objid_val } }, omit, '1234'O };
  enc_exp := '11000000011000000000000000100000011000000010111010101000000100001001000110100000'B;
  enc := enc_ext(x5);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #5 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x6 := { { context_negotiation := { 117, objid_val } }, "abc", '1234'O };
  enc_exp := '1110000001100000000000000010000001100000001011101010000001101100001011000100110001101000000100001001000110100000'B;
  enc := enc_ext(x6);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #6 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  setverdict(pass);
}

testcase tc_per_external_encode_aligned() runs on CT {
  var External x1 := { { syntax := objid_val }, omit, '1234'O };
  var bitstring enc_exp := '100000000000001100000000000000010000001101000000000000100001001000110100'B;
  var bitstring enc := enc_ext_a(x1);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #1 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x2 := { { syntax := objid_val }, "abc", '1234'O };
  enc_exp := '10100000000000110000000000000001000000110000001101100001011000100110001101000000000000100001001000110100'B;
  enc := enc_ext_a(x2);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #2 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x3 := { { presentation_context_id := 117 }, omit, '1234'O };
  enc_exp := '01000000000000010111010101000000000000100001001000110100'B;
  enc := enc_ext_a(x3);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #3 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x4 := { { presentation_context_id := 117 }, "abc", '1234'O };
  enc_exp := '0110000000000001011101010000001101100001011000100110001101000000000000100001001000110100'B;
  enc := enc_ext_a(x4);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #4 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x5 := { { context_negotiation := { 117, objid_val } }, omit, '1234'O };
  enc_exp := '1100000000000011000000000000000100000011000000010111010101000000000000100001001000110100'B;
  enc := enc_ext_a(x5);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #5 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var External x6 := { { context_negotiation := { 117, objid_val } }, "abc", '1234'O };
  enc_exp := '111000000000001100000000000000010000001100000001011101010000001101100001011000100110001101000000000000100001001000110100'B;
  enc := enc_ext_a(x6);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding external #6 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  setverdict(pass);
}

testcase tc_per_external_decode_unaligned() runs on CT {
  var bitstring enc := '1000000001100000000000000010000001101000000100001001000110100000'B;
  var External x1 := dec_ext(enc);
  var External x1_exp := { { syntax := objid_val }, omit, '1234'O };
  if (x1 != x1_exp) {
    setverdict(fail, "Decoding external #1 failed. Got: ", x1, ", expected: ", x1_exp);
  }
  enc := '101000000110000000000000001000000110000001101100001011000100110001101000000100001001000110100000'B;
  var External x2 := dec_ext(enc);
  var External x2_exp := { { syntax := objid_val }, "abc", '1234'O };
  if (x2 != x2_exp) {
    setverdict(fail, "Decoding external #2 failed. Got: ", x2, ", expected: ", x2_exp);
  }
  enc := '010000000010111010101000000100001001000110100000'B;
  var External x3 := dec_ext(enc);
  var External x3_exp := { { presentation_context_id := 117 }, omit, '1234'O };
  if (x3 != x3_exp) {
    setverdict(fail, "Decoding external #3 failed. Got: ", x3, ", expected: ", x3_exp);
  }
  enc := '01100000001011101010000001101100001011000100110001101000000100001001000110100000'B;
  var External x4 := dec_ext(enc);
  var External x4_exp := { { presentation_context_id := 117 }, "abc", '1234'O };
  if (x4 != x4_exp) {
    setverdict(fail, "Decoding external #4 failed. Got: ", x4, ", expected: ", x4_exp);
  }
  enc := '11000000011000000000000000100000011000000010111010101000000100001001000110100000'B;
  var External x5 := dec_ext(enc);
  var External x5_exp := { { context_negotiation := { 117, objid_val } }, omit, '1234'O };
  if (x5 != x5_exp) {
    setverdict(fail, "Decoding external #5 failed. Got: ", x5, ", expected: ", x5_exp);
  }
  enc := '1110000001100000000000000010000001100000001011101010000001101100001011000100110001101000000100001001000110100000'B;
  var External x6 := dec_ext(enc);
  var External x6_exp := { { context_negotiation := { 117, objid_val } }, "abc", '1234'O };
  if (x6 != x6_exp) {
    setverdict(fail, "Decoding external #6 failed. Got: ", x6, ", expected: ", x6_exp);
  }
  setverdict(pass);
}

testcase tc_per_external_decode_aligned() runs on CT {
  var bitstring enc := '100000000000001100000000000000010000001101000000000000100001001000110100'B;
  var External x1 := dec_ext_a(enc);
  var External x1_exp := { { syntax := objid_val }, omit, '1234'O };
  if (x1 != x1_exp) {
    setverdict(fail, "Decoding external #1 failed. Got: ", x1, ", expected: ", x1_exp);
  }
  enc := '10100000000000110000000000000001000000110000001101100001011000100110001101000000000000100001001000110100'B;
  var External x2 := dec_ext_a(enc);
  var External x2_exp := { { syntax := objid_val }, "abc", '1234'O };
  if (x2 != x2_exp) {
    setverdict(fail, "Decoding external #2 failed. Got: ", x2, ", expected: ", x2_exp);
  }
  enc := '01000000000000010111010101000000000000100001001000110100'B;
  var External x3 := dec_ext_a(enc);
  var External x3_exp := { { presentation_context_id := 117 }, omit, '1234'O };
  if (x3 != x3_exp) {
    setverdict(fail, "Decoding external #3 failed. Got: ", x3, ", expected: ", x3_exp);
  }
  enc := '0110000000000001011101010000001101100001011000100110001101000000000000100001001000110100'B;
  var External x4 := dec_ext_a(enc);
  var External x4_exp := { { presentation_context_id := 117 }, "abc", '1234'O };
  if (x4 != x4_exp) {
    setverdict(fail, "Decoding external #4 failed. Got: ", x4, ", expected: ", x4_exp);
  }
  enc := '1100000000000011000000000000000100000011000000010111010101000000000000100001001000110100'B;
  var External x5 := dec_ext_a(enc);
  var External x5_exp := { { context_negotiation := { 117, objid_val } }, omit, '1234'O };
  if (x5 != x5_exp) {
    setverdict(fail, "Decoding external #5 failed. Got: ", x5, ", expected: ", x5_exp);
  }
  enc := '111000000000001100000000000000010000001100000001011101010000001101100001011000100110001101000000000000100001001000110100'B;
  var External x6 := dec_ext_a(enc);
  var External x6_exp := { { context_negotiation := { 117, objid_val } }, "abc", '1234'O };
  if (x6 != x6_exp) {
    setverdict(fail, "Decoding external #6 failed. Got: ", x6, ", expected: ", x6_exp);
  }
  setverdict(pass);
}

testcase tc_per_external_negtest() runs on CT {
  var External x;
  var bitstring buf;
  @try {
    buf := enc_ext(x);
    setverdict(fail, "External #1 encoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Encoding an unbound EXTERNAL value.")) {
      setverdict(fail, "Invalid error message received (#1): ", msg);
    }
  }
  buf := ''B;
  @try {
    x := dec_ext(buf);
    setverdict(fail, "External #2 decoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Buffer overflow while PER-decoding. Requested bits: 2, remaining bits in buffer: 0")) {
      setverdict(fail, "Invalid error message received (#2): ", msg);
    }
  }
  buf := '00000000'B;
  @try {
    x := dec_ext(buf);
    setverdict(fail, "External #3 decoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Invalid optional bit-map decoded for EXTERNAL value. The 'direct-reference' and 'indirect-reference' fields can't both be omitted.")) {
      setverdict(fail, "Invalid error message received (#3): ", msg);
    }
  }
  buf := '010000000010000000010000'B;
  @try {
    x := dec_ext(buf);
    setverdict(fail, "External #4 decoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Decoded invalid CHOICE selection in EXTERNAL value. Only the 'octet-aligned' alternative is supported.")) {
      setverdict(fail, "Invalid error message received (#4): ", msg);
    }
  }
  setverdict(pass);
}
  
control {
  execute(tc_per_external_encode_unaligned());
  execute(tc_per_external_encode_aligned());
  execute(tc_per_external_decode_unaligned());
  execute(tc_per_external_decode_aligned());
  execute(tc_per_external_negtest());
}

}
