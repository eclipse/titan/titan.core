/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Baranyi, Botond – initial implementation
 *
 ******************************************************************************/
module IntegerTests {

import from Types all;
import from Tools all;

// unaligned
external function enc_int1(in Int1 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int2(in Int2 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int3(in Int3 x) return bitstring with { extension "prototype(convert) encode(PER:UNALIGNED) errorbehavior(ALL:ERROR)" }
external function enc_int4(in Int4 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int5(in Int5 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int6(in Int6 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int7(in Int7 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int8(in Int8 x) return bitstring with { extension "prototype(convert) encode(PER:nothing)" }
external function enc_int9(in Int9 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int10(in Int10 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int11(in Int11 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int12(in Int12 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int13(in Int13 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int14(in Int14 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int15(in Int15 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int16(in Int16 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int17(in Int17 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int18(in Int18 x) return bitstring with { extension "prototype(convert) encode(PER)" }
external function enc_int19(in Int19 x) return bitstring with { extension "prototype(convert) encode(PER)" }

external function dec_int1(in bitstring x) return Int1 with { extension "prototype(convert) decode(PER)" }
external function dec_int2(in bitstring x) return Int2 with { extension "prototype(convert) decode(PER)" }
external function dec_int3(in bitstring x) return Int3 with { extension "prototype(convert) decode(PER:UNALIGNED) errorbehavior(ALL:ERROR)" }
external function dec_int4(in bitstring x) return Int4 with { extension "prototype(convert) decode(PER)" }
external function dec_int5(in bitstring x) return Int5 with { extension "prototype(convert) decode(PER)" }
external function dec_int6(in bitstring x) return Int6 with { extension "prototype(convert) decode(PER)" }
external function dec_int7(in bitstring x) return Int7 with { extension "prototype(convert) decode(PER)" }
external function dec_int8(in bitstring x) return Int8 with { extension "prototype(convert) decode(PER:nothing)" }
external function dec_int9(in bitstring x) return Int9 with { extension "prototype(convert) decode(PER)" }
external function dec_int10(in bitstring x) return Int10 with { extension "prototype(convert) decode(PER) errorbehavior(ALL:ERROR)" }
external function dec_int11(in bitstring x) return Int11 with { extension "prototype(convert) decode(PER)" }
external function dec_int12(in bitstring x) return Int12 with { extension "prototype(convert) decode(PER)" }
external function dec_int13(in bitstring x) return Int13 with { extension "prototype(convert) decode(PER)" }
external function dec_int14(in bitstring x) return Int14 with { extension "prototype(convert) decode(PER)" }
external function dec_int15(in bitstring x) return Int15 with { extension "prototype(convert) decode(PER)" }
external function dec_int16(in bitstring x) return Int16 with { extension "prototype(convert) decode(PER)" }
external function dec_int17(in bitstring x) return Int17 with { extension "prototype(convert) decode(PER)" }
external function dec_int18(in bitstring x) return Int18 with { extension "prototype(convert) decode(PER)" }
external function dec_int19(in bitstring x) return Int19 with { extension "prototype(convert) decode(PER)" }

// aligned
external function enc_int1_a(in Int1 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int2_a(in Int2 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int3_a(in Int3 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int4_a(in Int4 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int5_a(in Int5 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int6_a(in Int6 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int7_a(in Int7 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int8_a(in Int8 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int9_a(in Int9 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int10_a(in Int10 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int11_a(in Int11 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int12_a(in Int12 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int13_a(in Int13 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int14_a(in Int14 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int15_a(in Int15 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int16_a(in Int16 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int17_a(in Int17 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int18_a(in Int18 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }
external function enc_int19_a(in Int19 x) return bitstring with { extension "prototype(convert) encode(PER:ALIGNED)" }

external function dec_int1_a(in bitstring x) return Int1 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int2_a(in bitstring x) return Int2 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int3_a(in bitstring x) return Int3 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int4_a(in bitstring x) return Int4 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int5_a(in bitstring x) return Int5 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int6_a(in bitstring x) return Int6 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int7_a(in bitstring x) return Int7 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int8_a(in bitstring x) return Int8 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int9_a(in bitstring x) return Int9 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int10_a(in bitstring x) return Int10 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int11_a(in bitstring x) return Int11 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int12_a(in bitstring x) return Int12 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int13_a(in bitstring x) return Int13 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int14_a(in bitstring x) return Int14 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int15_a(in bitstring x) return Int15 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int16_a(in bitstring x) return Int16 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int17_a(in bitstring x) return Int17 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int18_a(in bitstring x) return Int18 with { extension "prototype(convert) decode(PER:ALIGNED)" }
external function dec_int19_a(in bitstring x) return Int19 with { extension "prototype(convert) decode(PER:ALIGNED)" }

testcase tc_per_integer_encode_unaligned() runs on CT {
  // single value
  var Int1 x1 := 21;
  var bitstring enc_exp := ''B;
  var bitstring enc := enc_int1(x1);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #1 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // constrained value range
  var Int2 x2 := 14;
  enc_exp := '01000000'B;
  enc := enc_int2(x2);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #2 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int3 x3 := 17;
  enc_exp := '0000000011100000'B;
  enc := enc_int3(x3);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #3 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int4 x4 := 3592;
  enc_exp := '000000000110111111110000'B;
  enc := enc_int4(x4);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #4 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int5 x5 := 123145302310920;
  enc_exp := '00000001101111111111111111111111111111111111111111111000'B;
  enc := enc_int5(x5);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #5 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int6 x6 := 3848290697224;
  enc_exp := '000110111111111111111111111111111111111111110000'B;
  enc := enc_int6(x6);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #6 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int7 x7 := 962072674312;
  enc_exp := '1101111111111111111111111111111111111110'B;
  enc := enc_int7(x7);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #7 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int8 x8 := 962072674312;
  enc_exp := '000000001101111111111111111111111111111111111110'B;
  enc := enc_int8(x8);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #8 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // semi-constrained value range
  var Int9 x9 := 3592;
  enc_exp := '000000100000110111111110'B;
  enc := enc_int9(x9);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #9 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var octetstring oct := f_DFE(130, false);
  var Int9 x10 := oct2int(oct) + 10;
  enc_exp := '1000000010000010'B & oct2bit(oct);
  enc := enc_int9(x10);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #10 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // unconstrained value range
  var Int10 x11 := -19;
  enc_exp := '0000000111101101'B;
  enc := enc_int10(x11);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #11 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int10 x11_big := -60129542142;
  enc_exp := '000001011111001000000000000000000000000000000010'B;
  enc := enc_int10_a(x11_big);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #11 (big) failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int11 x12 := 3582;
  enc_exp := '000000100000110111111110'B;
  enc := enc_int11(x12);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #12 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  oct := f_DFE(9 * c_16K + 1, false);
  var Int11 x13 := oct2int(oct);
  enc_exp := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  enc := enc_int11(x13);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #13 (positive) failed."); // don't log 2 million-length bitstrings
  }
  oct := f_DFE(9 * c_16K + 1, true);
  var Int11 x13_neg := (oct2int(not4b(oct)) + 1) * -1;
  enc_exp := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  enc := enc_int11_a(x13_neg);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #13 (negative) failed.");
  }
  var Int11 x14 := 41513;
  enc_exp := '00000011000000001010001000101001'B;
  enc := enc_int11(x14);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #14 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // extendable
  var Int12 x15 := 3592;
  enc_exp := '000000000011011111111000'B;
  enc := enc_int12(x15);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #15 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int12 x16 := -314;
  enc_exp := '10000001011111110110001100000000'B;
  enc := enc_int12(x16);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #16 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int13 x17 := 3592;
  enc_exp := '00000001000001101111111100000000'B;
  enc := enc_int13(x17);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #17 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int13 x18 := -314;
  enc_exp := '10000001011111110110001100000000'B;
  enc := enc_int13(x18);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #18 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int14 x19 := 32;
  enc_exp := '000000001001000000000000'B;
  enc := enc_int14(x19);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #19 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int14 x20 := 14680062;
  enc_exp := '100000100000000001101111111111111111111100000000'B;
  enc := enc_int14(x20);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #20 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int15 x21 := 42;
  enc_exp := '00000000'B;
  enc := enc_int15(x21);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #21 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int15 x22 := 11;
  enc_exp := '100000001000010110000000'B;
  enc := enc_int15(x22);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #22 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int16 x23 := 11;
  enc_exp := '000000001000010110000000'B;
  enc := enc_int16(x23);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #23 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int17 x24 := 11;
  enc_exp := '00001010'B;
  enc := enc_int17(x24);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #24 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int18 x25 := 11;
  enc_exp := '0000010100000000'B;
  enc := enc_int18(x25);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #25 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int19 x26 := 11;
  enc_exp := '0000000010100000'B;
  enc := enc_int19(x26);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #26 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  setverdict(pass);
}

testcase tc_per_integer_encode_aligned() runs on CT {
  // single value
  var Int1 x1 := 21;
  var bitstring enc_exp := ''B;
  var bitstring enc := enc_int1_a(x1);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #1 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // constrained value range
  var Int2 x2 := 14;
  enc_exp := '01000000'B;
  enc := enc_int2_a(x2);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #2 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int3 x3 := 17;
  enc_exp := '0000000000000111'B;
  enc := enc_int3_a(x3);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #3 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int4 x4 := 3592;
  enc_exp := '010000000000110111111110'B;
  enc := enc_int4_a(x4);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #4 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int5 x5 := 123145302310920;
  enc_exp := '10100000011011111111111111111111111111111111111111111110'B;
  enc := enc_int5_a(x5);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #5 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int6 x6 := 3848290697224;
  enc_exp := '10100000000000110111111111111111111111111111111111111110'B;
  enc := enc_int6_a(x6);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #6 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int7 x7 := 962072674312;
  enc_exp := '100000001101111111111111111111111111111111111110'B;
  enc := enc_int7_a(x7);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #7 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int8 x8 := 962072674312;
  enc_exp := '100000001101111111111111111111111111111111111110'B;
  enc := enc_int8_a(x8);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #8 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // semi-constrained value range
  var Int9 x9 := 3592;
  enc_exp := '000000100000110111111110'B;
  enc := enc_int9_a(x9);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #9 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var octetstring oct := f_DFE(130, false);
  var Int9 x10 := oct2int(oct) + 10;
  enc_exp := '1000000010000010'B & oct2bit(oct);
  enc := enc_int9_a(x10);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #10 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // unconstrained value range
  var Int10 x11 := -19;
  enc_exp := '0000000111101101'B;
  enc := enc_int10_a(x11);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #11 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int10 x11_big := -60129542142;
  enc_exp := '000001011111001000000000000000000000000000000010'B;
  enc := enc_int10_a(x11_big);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #11 (big) failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int11 x12 := 3582;
  enc_exp := '000000100000110111111110'B;
  enc := enc_int11_a(x12);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #12 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  oct := f_DFE(9 * c_16K + 1, false);
  var Int11 x13 := oct2int(oct);
  enc_exp := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  enc := enc_int11_a(x13);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #13 (positive) failed."); // don't log 2 million-length bitstrings
  }
  oct := f_DFE(9 * c_16K + 1, true);
  var Int11 x13_neg := (oct2int(not4b(oct)) + 1) * -1;
  enc_exp := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  enc := enc_int11_a(x13_neg);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #13 (negative) failed.");
  }
  var Int11 x14 := 41513;
  enc_exp := '00000011000000001010001000101001'B;
  enc := enc_int11_a(x14);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #14 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  
  // extendable
  var Int12 x15 := 3592;
  enc_exp := '001000000000110111111110'B;
  enc := enc_int12_a(x15);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #15 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int12 x16 := -314;
  enc_exp := '10000000000000101111111011000110'B;
  enc := enc_int12_a(x16);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #16 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int13 x17 := 3592;
  enc_exp := '00000000000000100000110111111110'B;
  enc := enc_int13_a(x17);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #17 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int13 x18 := -314;
  enc_exp := '10000000000000101111111011000110'B;
  enc := enc_int13_a(x18);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #18 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int14 x19 := 32;
  enc_exp := '000000000000000100100000'B;
  enc := enc_int14_a(x19);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #19 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int14 x20 := 14680062;
  enc_exp := '100000000000010000000000110111111111111111111110'B;
  enc := enc_int14_a(x20);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #20 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int15 x21 := 42;
  enc_exp := '00000000'B;
  enc := enc_int15_a(x21);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #21 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int15 x22 := 11;
  enc_exp := '100000000000000100001011'B;
  enc := enc_int15_a(x22);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #22 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int16 x23 := 11;
  enc_exp := '000000000000000100001011'B;
  enc := enc_int16_a(x23);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #23 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int17 x24 := 11;
  enc_exp := '00001010'B;
  enc := enc_int17_a(x24);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #24 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int18 x25 := 11;
  enc_exp := '0000000000001010'B;
  enc := enc_int18_a(x25);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #25 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  var Int19 x26 := 11;
  enc_exp := '000000000000000000001010'B;
  enc := enc_int19_a(x26);
  if (enc != enc_exp) {
    setverdict(fail, "Encoding integer #26 failed. Got: ", enc, ", expected: ", enc_exp);
  }
  setverdict(pass);
}

testcase tc_per_integer_decode_unaligned() runs on CT {
  // single value
  var bitstring enc := ''B;
  var Int1 x1 := dec_int1(enc);
  var Int1 x1_exp := 21;
  if (x1 != x1_exp) {
    setverdict(fail, "Decoding integer #1 failed. Got: ", x1, ", expected: ", x1_exp);
  }
  
  // constrained value range
  enc := '01000000'B;
  var Int2 x2 := dec_int2(enc);
  var Int2 x2_exp := 14;
  if (x2 != x2_exp) {
    setverdict(fail, "Decoding integer #2 failed. Got: ", x2, ", expected: ", x2_exp);
  }
  enc := '0000000011100000'B;
  var Int3 x3 := dec_int3(enc);
  var Int3 x3_exp := 17;
  if (x3 != x3_exp) {
    setverdict(fail, "Decoding integer #3 failed. Got: ", x3, ", expected: ", x3_exp);
  }
  enc := '000000000110111111110000'B;
  var Int4 x4 := dec_int4(enc);
  var Int4 x4_exp := 3592;
  if (x4 != x4_exp) {
    setverdict(fail, "Decoding integer #4 failed. Got: ", x4, ", expected: ", x4_exp);
  }
  enc := '00000001101111111111111111111111111111111111111111111000'B;
  var Int5 x5 := dec_int5(enc);
  var Int5 x5_exp := 123145302310920;
  if (x5 != x5_exp) {
    setverdict(fail, "Decoding integer #5 failed. Got: ", x5, ", expected: ", x5_exp);
  }
  enc := '000110111111111111111111111111111111111111110000'B;
  var Int6 x6 := dec_int6(enc);
  var Int6 x6_exp := 3848290697224;
  if (x6 != x6_exp) {
    setverdict(fail, "Decoding integer #6 failed. Got: ", x6, ", expected: ", x6_exp);
  }
  enc := '1101111111111111111111111111111111111110'B;
  var Int7 x7 := dec_int7(enc);
  var Int7 x7_exp := 962072674312;
  if (x7 != x7_exp) {
    setverdict(fail, "Decoding integer #7 failed. Got: ", x7, ", expected: ", x7_exp);
  }
  enc := '000000001101111111111111111111111111111111111110'B;
  var Int8 x8 := dec_int8(enc);
  var Int8 x8_exp := 962072674312;
  if (x8 != x8_exp) {
    setverdict(fail, "Decoding integer #8 failed. Got: ", x8, ", expected: ", x8_exp);
  }
  
  // semi-constrained value range
  enc := '000000100000110111111110'B;
  var Int9 x9 := dec_int9(enc);
  var Int9 x9_exp := 3592;
  if (x9 != x9_exp) {
    setverdict(fail, "Decoding integer #9 failed. Got: ", x9, ", expected: ", x9_exp);
  }
  var octetstring oct := f_DFE(130, false);
  enc := '1000000010000010'B & oct2bit(oct);
  var Int9 x10 := dec_int9(enc);
  var Int9 x10_exp := oct2int(oct) + 10;
  if (x10 != x10_exp) {
    setverdict(fail, "Decoding integer #10 failed. Got: ", x10, ", expected: ", x10_exp);
  }
  
  // unconstrained value range
  enc := '0000000111101101'B;
  var Int10 x11 := dec_int10(enc);
  var Int10 x11_exp := -19;
  if (x11 != x11_exp) {
    setverdict(fail, "Decoding integer #11 failed. Got: ", x11, ", expected: ", x11_exp);
  }
  enc := '000001011111001000000000000000000000000000000010'B;
  var Int10 x11_big := dec_int10_a(enc);
  var Int10 x11_big_exp := -60129542142;
  if (x11_big != x11_big_exp) {
    setverdict(fail, "Decoding integer #11 (big) failed. Got: ", x11_big, ", expected: ", x11_big_exp);
  }
  enc := '000000100000110111111110'B;
  var Int11 x12 := dec_int11(enc);
  var Int11 x12_exp := 3582;
  if (x12 != x12_exp) {
    setverdict(fail, "Decoding integer #12 failed. Got: ", x12, ", expected: ", x12_exp);
  }
  oct := f_DFE(9 * c_16K + 1, false);
  enc := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  var Int11 x13_pos := dec_int11_a(enc);
  var Int11 x13_pos_exp := oct2int(oct);
  if (x13_pos != x13_pos_exp) {
    setverdict(fail, "Decoding integer #13 (positive) failed.");
  }
  oct := f_DFE(9 * c_16K + 1, true);
  enc := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  var Int11 x13_neg := dec_int11_a(enc);
  var Int11 x13_neg_exp := (oct2int(not4b(oct)) + 1) * -1;
  if (x13_neg != x13_neg_exp) {
    setverdict(fail, "Decoding integer #13 (negative) failed.");
  }
  enc := '00000011000000001010001000101001'B;
  var Int11 x14 := dec_int11(enc);
  var Int11 x14_exp := 41513;
  if (x14 != x14_exp) {
    setverdict(fail, "Decoding integer #14 failed. Got: ", x14, ", expected: ", x14_exp);
  }
  
  // extendable
  enc := '000000000011011111111000'B;
  var Int12 x15 := dec_int12(enc);
  var Int12 x15_exp := 3592;
  if (x15 != x15_exp) {
    setverdict(fail, "Decoding integer #15 failed. Got: ", x15, ", expected: ", x15_exp);
  }
  enc := '10000001011111110110001100000000'B;
  var Int12 x16 := dec_int12(enc);
  var Int12 x16_exp := -314;
  if (x16 != x16_exp) {
    setverdict(fail, "Decoding integer #16 failed. Got: ", x16, ", expected: ", x16_exp);
  }
  enc := '00000001000001101111111100000000'B;
  var Int13 x17 := dec_int13(enc);
  var Int13 x17_exp := 3592;
  if (x17 != x17_exp) {
    setverdict(fail, "Decoding integer #17 failed. Got: ", x17, ", expected: ", x17_exp);
  }
  enc := '10000001011111110110001100000000'B;
  var Int13 x18 := dec_int13(enc);
  var Int13 x18_exp := -314;
  if (x18 != x18_exp) {
    setverdict(fail, "Decoding integer #18 failed. Got: ", x18, ", expected: ", x18_exp);
  }
  enc := '000000001001000000000000'B;
  var Int14 x19 := dec_int14(enc);
  var Int14 x19_exp := 32;
  if (x19 != x19_exp) {
    setverdict(fail, "Decoding integer #19 failed. Got: ", x19, ", expected: ", x19_exp);
  }
  enc := '100000100000000001101111111111111111111100000000'B;
  var Int14 x20 := dec_int14(enc);
  var Int14 x20_exp := 14680062;
  if (x20 != x20_exp) {
    setverdict(fail, "Decoding integer #20 failed. Got: ", x20, ", expected: ", x20_exp);
  }
  enc := '00000000'B;
  var Int15 x21 := dec_int15(enc);
  var Int15 x21_exp := 42;
  if (x21 != x21_exp) {
    setverdict(fail, "Decoding integer #21 failed. Got: ", x21, ", expected: ", x21_exp);
  }
  enc := '100000001000010110000000'B;
  var Int15 x22 := dec_int15(enc);
  var Int15 x22_exp := 11;
  if (x22 != x22_exp) {
    setverdict(fail, "Decoding integer #22 failed. Got: ", x22, ", expected: ", x22_exp);
  }
  enc := '000000001000010110000000'B;
  var Int16 x23 := dec_int16(enc);
  var Int16 x23_exp := 11;
  if (x23 != x23_exp) {
    setverdict(fail, "Decoding integer #23 failed. Got: ", x23, ", expected: ", x23_exp);
  }
  enc := '00001010'B;
  var Int17 x24 := dec_int17(enc);
  var Int17 x24_exp := 11;
  if (x24 != x24_exp) {
    setverdict(fail, "Decoding integer #24 failed. Got: ", x24, ", expected: ", x24_exp);
  }
  enc := '0000010100000000'B;
  var Int18 x25 := dec_int18(enc);
  var Int18 x25_exp := 11;
  if (x25 != x25_exp) {
    setverdict(fail, "Decoding integer #25 failed. Got: ", x25, ", expected: ", x25_exp);
  }
  enc := '0000000010100000'B;
  var Int19 x26 := dec_int19(enc);
  var Int19 x26_exp := 11;
  if (x26 != x26_exp) {
    setverdict(fail, "Decoding integer #26 failed. Got: ", x26, ", expected: ", x26_exp);
  }
  
  // decoding an unconstrained integer with zero length
  enc := '00000000'B;
  var Int11 x27 := dec_int11(enc);
  var Int11 x27_exp := 0;
  if (x27 != x27_exp) {
    setverdict(fail, "Decoding integer #27 failed. Got: ", x27, ", expected: ", x26_exp);
  }
  setverdict(pass);
}

testcase tc_per_integer_decode_aligned() runs on CT {
  // single value
  var bitstring enc := ''B;
  var Int1 x1 := dec_int1_a(enc);
  var Int1 x1_exp := 21;
  if (x1 != x1_exp) {
    setverdict(fail, "Decoding integer #1 failed. Got: ", x1, ", expected: ", x1_exp);
  }
  
  // constrained value range
  enc := '01000000'B;
  var Int2 x2 := dec_int2_a(enc);
  var Int2 x2_exp := 14;
  if (x2 != x2_exp) {
    setverdict(fail, "Decoding integer #2 failed. Got: ", x2, ", expected: ", x2_exp);
  }
  enc := '0000000000000111'B;
  var Int3 x3 := dec_int3_a(enc);
  var Int3 x3_exp := 17;
  if (x3 != x3_exp) {
    setverdict(fail, "Decoding integer #3 failed. Got: ", x3, ", expected: ", x3_exp);
  }
  enc := '010000000000110111111110'B;
  var Int4 x4 := dec_int4_a(enc);
  var Int4 x4_exp := 3592;
  if (x4 != x4_exp) {
    setverdict(fail, "Decoding integer #4 failed. Got: ", x4, ", expected: ", x4_exp);
  }
  enc := '10100000011011111111111111111111111111111111111111111110'B;
  var Int5 x5 := dec_int5_a(enc);
  var Int5 x5_exp := 123145302310920;
  if (x5 != x5_exp) {
    setverdict(fail, "Decoding integer #5 failed. Got: ", x5, ", expected: ", x5_exp);
  }
  enc := '10100000000000110111111111111111111111111111111111111110'B;
  var Int6 x6 := dec_int6_a(enc);
  var Int6 x6_exp := 3848290697224;
  if (x6 != x6_exp) {
    setverdict(fail, "Decoding integer #6 failed. Got: ", x6, ", expected: ", x6_exp);
  }
  enc := '100000001101111111111111111111111111111111111110'B;
  var Int7 x7 := dec_int7_a(enc);
  var Int7 x7_exp := 962072674312;
  if (x7 != x7_exp) {
    setverdict(fail, "Decoding integer #7 failed. Got: ", x7, ", expected: ", x7_exp);
  }
  enc := '100000001101111111111111111111111111111111111110'B;
  var Int8 x8 := dec_int8_a(enc);
  var Int8 x8_exp := 962072674312;
  if (x8 != x8_exp) {
    setverdict(fail, "Decoding integer #8 failed. Got: ", x8, ", expected: ", x8_exp);
  }
  
  // semi-constrained value range
  enc := '000000100000110111111110'B;
  var Int9 x9 := dec_int9_a(enc);
  var Int9 x9_exp := 3592;
  if (x9 != x9_exp) {
    setverdict(fail, "Decoding integer #9 failed. Got: ", x9, ", expected: ", x9_exp);
  }
  var octetstring oct := f_DFE(130, false);
  enc := '1000000010000010'B & oct2bit(oct);
  var Int9 x10 := dec_int9_a(enc);
  var Int9 x10_exp := oct2int(oct) + 10;
  if (x10 != x10_exp) {
    setverdict(fail, "Decoding integer #10 failed. Got: ", x10, ", expected: ", x10_exp);
  }
  
  // unconstrained value range
  enc := '0000000111101101'B;
  var Int10 x11 := dec_int10_a(enc);
  var Int10 x11_exp := -19;
  if (x11 != x11_exp) {
    setverdict(fail, "Decoding integer #11 failed. Got: ", x11, ", expected: ", x11_exp);
  }
  enc := '000001011111001000000000000000000000000000000010'B;
  var Int10 x11_big := dec_int10_a(enc);
  var Int10 x11_big_exp := -60129542142;
  if (x11_big != x11_big_exp) {
    setverdict(fail, "Decoding integer #11 (big) failed. Got: ", x11_big, ", expected: ", x11_big_exp);
  }
  enc := '000000100000110111111110'B;
  var Int11 x12 := dec_int11_a(enc);
  var Int11 x12_exp := 3582;
  if (x12 != x12_exp) {
    setverdict(fail, "Decoding integer #12 failed. Got: ", x12, ", expected: ", x12_exp);
  }
  oct := f_DFE(9 * c_16K + 1, false);
  enc := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  var Int11 x13_pos := dec_int11_a(enc);
  var Int11 x13_pos_exp := oct2int(oct);
  if (x13_pos != x13_pos_exp) {
    setverdict(fail, "Decoding integer #13 (positive) failed.");
  }
  oct := f_DFE(9 * c_16K + 1, true);
  enc := '11000100'B & oct2bit(substr(oct, 0, 4 * c_16K)) &
    '11000100'B & oct2bit(substr(oct, 4 * c_16K, 4 * c_16K)) &
    '11000001'B & oct2bit(substr(oct, 8 * c_16K, c_16K)) &
    '00000001'B & oct2bit(oct[lengthof(oct) - 1]);
  var Int11 x13_neg := dec_int11_a(enc);
  var Int11 x13_neg_exp := (oct2int(not4b(oct)) + 1) * -1;
  if (x13_neg != x13_neg_exp) {
    setverdict(fail, "Decoding integer #13 (negative) failed.");
  }
  enc := '00000011000000001010001000101001'B;
  var Int11 x14 := dec_int11_a(enc);
  var Int11 x14_exp := 41513;
  if (x14 != x14_exp) {
    setverdict(fail, "Decoding integer #14 failed. Got: ", x14, ", expected: ", x14_exp);
  }
  
  // extendable
  enc := '001000000000110111111110'B;
  var Int12 x15 := dec_int12_a(enc);
  var Int12 x15_exp := 3592;
  if (x15 != x15_exp) {
    setverdict(fail, "Decoding integer #15 failed. Got: ", x15, ", expected: ", x15_exp);
  }
  enc := '10000000000000101111111011000110'B;
  var Int12 x16 := dec_int12_a(enc);
  var Int12 x16_exp := -314;
  if (x16 != x16_exp) {
    setverdict(fail, "Decoding integer #16 failed. Got: ", x16, ", expected: ", x16_exp);
  }
  enc := '00000000000000100000110111111110'B;
  var Int13 x17 := dec_int13_a(enc);
  var Int13 x17_exp := 3592;
  if (x17 != x17_exp) {
    setverdict(fail, "Decoding integer #17 failed. Got: ", x17, ", expected: ", x17_exp);
  }
  enc := '10000000000000101111111011000110'B;
  var Int13 x18 := dec_int13_a(enc);
  var Int13 x18_exp := -314;
  if (x18 != x18_exp) {
    setverdict(fail, "Decoding integer #18 failed. Got: ", x18, ", expected: ", x18_exp);
  }
  enc := '000000000000000100100000'B;
  var Int14 x19 := dec_int14_a(enc);
  var Int14 x19_exp := 32;
  if (x19 != x19_exp) {
    setverdict(fail, "Decoding integer #19 failed. Got: ", x19, ", expected: ", x19_exp);
  }
  enc := '100000000000010000000000110111111111111111111110'B;
  var Int14 x20 := dec_int14_a(enc);
  var Int14 x20_exp := 14680062;
  if (x20 != x20_exp) {
    setverdict(fail, "Decoding integer #20 failed. Got: ", x20, ", expected: ", x20_exp);
  }
  enc := '00000000'B;
  var Int15 x21 := dec_int15_a(enc);
  var Int15 x21_exp := 42;
  if (x21 != x21_exp) {
    setverdict(fail, "Decoding integer #21 failed. Got: ", x21, ", expected: ", x21_exp);
  }
  enc := '100000000000000100001011'B;
  var Int15 x22 := dec_int15_a(enc);
  var Int15 x22_exp := 11;
  if (x22 != x22_exp) {
    setverdict(fail, "Decoding integer #22 failed. Got: ", x22, ", expected: ", x22_exp);
  }
  enc := '000000000000000100001011'B;
  var Int16 x23 := dec_int16_a(enc);
  var Int16 x23_exp := 11;
  if (x23 != x23_exp) {
    setverdict(fail, "Decoding integer #23 failed. Got: ", x23, ", expected: ", x23_exp);
  }
  enc := '00001010'B;
  var Int17 x24 := dec_int17_a(enc);
  var Int17 x24_exp := 11;
  if (x24 != x24_exp) {
    setverdict(fail, "Decoding integer #24 failed. Got: ", x24, ", expected: ", x24_exp);
  }
  enc := '0000000000001010'B;
  var Int18 x25 := dec_int18_a(enc);
  var Int18 x25_exp := 11;
  if (x25 != x25_exp) {
    setverdict(fail, "Decoding integer #25 failed. Got: ", x25, ", expected: ", x25_exp);
  }
  enc := '000000000000000000001010'B;
  var Int19 x26 := dec_int19_a(enc);
  var Int19 x26_exp := 11;
  if (x26 != x26_exp) {
    setverdict(fail, "Decoding integer #26 failed. Got: ", x26, ", expected: ", x26_exp);
  }
  
  // decoding an unconstrained integer with zero length
  enc := '00000000'B;
  var Int11 x27 := dec_int11_a(enc);
  var Int11 x27_exp := 0;
  if (x27 != x27_exp) {
    setverdict(fail, "Decoding integer #27 failed. Got: ", x27, ", expected: ", x26_exp);
  }
  setverdict(pass);
}

testcase tc_per_integer_negtest() runs on CT {
  var integer x;
  var bitstring buf;
  @try {
    buf := enc_int3(x);
    setverdict(fail, "Integer #1 encoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Encoding an unbound integer value.")) {
      setverdict(fail, "Invalid error message received (#1): ", msg);
    }
  }
  x := 4000;
  @try {
    buf := enc_int3(x);
    setverdict(fail, "Integer #2 encoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Encoding an invalid integer value \(does not match PER-visible constraints\).")) {
      setverdict(fail, "Invalid error message received (#2): ", msg);
    }
  }
  buf := '1111111111100000'B;
  @try {
    x := dec_int3(buf);
    setverdict(fail, "Integer #3 decoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Decoded integer value does not match PER-visible constraints.")) {
      setverdict(fail, "Invalid error message received (#3): ", msg);
    }
  }
  buf := '0000000100000011'B;
  @try {
    x := dec_int10(buf);
    setverdict(fail, "Integer #4 decoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Decoded integer value does not match PER-visible constraints.")) {
      setverdict(fail, "Invalid error message received (#4): ", msg);
    }
  }
  buf := '000010100000111100001111'B;
  @try {
    x := dec_int11(buf);
    setverdict(fail, "Integer #5 decoded successfully.");
  }
  @catch (msg) {
    if (not match(msg, pattern "*Buffer overflow while PER-decoding. Requested bits: 80, remaining bits in buffer: 16")) {
      setverdict(fail, "Invalid error message received (#5): ", msg);
    }
  }
  setverdict(pass);
}

control {
  execute(tc_per_integer_encode_unaligned());
  execute(tc_per_integer_encode_aligned());
  execute(tc_per_integer_decode_unaligned());
  execute(tc_per_integer_decode_aligned());
  execute(tc_per_integer_negtest());
}

}
