/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Balasko, Jeno
 *   Kovacs, Ferenc
 *   Raduly, Csaba
 *   Szabados, Kristof
 *
 ******************************************************************************/
module TdefaultOper
{
type component defaultOper_comptype { };

altstep A1() runs on defaultOper_comptype
{
  [] any timer.timeout { setverdict(inconc);}
}

testcase defaultIsvalue() runs on defaultOper_comptype
{
  var default v0;
  var default v1 := null;
  var template default tdf0;
  template default tdf1 := null;
  var default d1 := activate( A1() );

  if ( isvalue(v0) ) { setverdict(fail); } else { setverdict(pass); };
  if ( isvalue(v1) ) { setverdict(pass); } else { setverdict(fail); };
  if ( isvalue(tdf0) ) { setverdict(fail); } else { setverdict(pass); };
  if ( isvalue(tdf1) ) { setverdict(pass); } else { setverdict(fail); };
  if ( isvalue(d1) ) { setverdict(pass); } else { setverdict(fail); };
}

  altstep as_empty() runs on defaultOper_comptype {
    [] any timer.timeout { }
  };

  function f_empty() runs on defaultOper_comptype { }

  testcase tc_special_default_value_unbound() runs on defaultOper_comptype {
    var default vl_default;
    if(ispresent(vl_default)){setverdict(fail)}else {setverdict(pass)};
  }

  testcase tc_special_default_value_null() runs on defaultOper_comptype {
    var default vl_default := null;
    if(ispresent(vl_default)) {setverdict(pass)} else {setverdict(fail)}
  }
  testcase tc_special_default_value_isvalue() runs on defaultOper_comptype {
    var default vl_default := activate(as_empty());
    if(ispresent(vl_default)){setverdict(pass)}else {setverdict(fail)};

    deactivate(vl_default);
    if(ispresent(vl_default)){setverdict(pass)}else {setverdict(fail)};
  }

  testcase tc_special_compref_unbound() runs on defaultOper_comptype {
    var defaultOper_comptype vc_myComp;
    if(ispresent(vc_myComp)){setverdict(fail)}else {setverdict(pass)};
  }

  testcase tc_special_compref_isvalue() runs on defaultOper_comptype {
    var defaultOper_comptype vc_myComp:= defaultOper_comptype.create("hali");
    if(ispresent(vc_myComp)){setverdict(pass)}else {setverdict(fail)};

    vc_myComp.start(f_empty());
    if(ispresent(vc_myComp)){setverdict(pass)}else {setverdict(fail)};

    vc_myComp.done;
    if(ispresent(vc_myComp)){setverdict(pass)}else {setverdict(fail)};
  }

testcase tc_special_default_value() runs on defaultOper_comptype {
  var default vl_default;
  if(isbound(vl_default)){setverdict(fail)}else {setverdict(pass)};

  vl_default := null;
  if(isbound(vl_default)) {setverdict(pass)} else {setverdict(fail)}

  vl_default := activate(as_empty());
  if(isbound(vl_default)){setverdict(pass)}else {setverdict(fail)};

  deactivate(vl_default);
  if(isbound(vl_default)){setverdict(pass)}else {setverdict(fail)};
}

testcase tc_special_compref() runs on defaultOper_comptype {
  var defaultOper_comptype vc_myComp;
  if(isbound(vc_myComp)){setverdict(fail)}else {setverdict(pass)};

  vc_myComp:= defaultOper_comptype.create("hali");
  if(isbound(vc_myComp)){setverdict(pass)}else {setverdict(fail)};

  vc_myComp.start(f_empty());
  if(isbound(vc_myComp)){setverdict(pass)}else {setverdict(fail)};
  
  vc_myComp.done;
  if(isbound(vc_myComp)){setverdict(pass)}else {setverdict(fail)};
}

type port PT message{
  inout integer;
} with { extension "internal"; }

type component CT {
  port PT p;
  var integer v;
}

altstep as(in integer par) runs on CT {
  [] p.receive { v := par; }
}

testcase tc_default_activate_deactivate() runs on CT {

  connect(self:p, self:p);

  var default v_def1 := activate(as(1));
  var default v_def2 := activate(as(2));

  p.send(1);
  p.receive(2); // altsteps will handle as 2 is not sent
  if (v != 2) {
    setverdict(fail, "#1: ", v);
  }

  deactivate(v_def2, true); // suspend only

  p.send(1);
  p.receive(2); // altsteps will handle as 2 is not sent
  if (v != 1) {
    setverdict(fail, "#2: ", v);
  }

  activate(v_def2); // reactivate suspended default

  p.send(1);
  p.receive(2); // altsteps will handle as 2 is not sent
  if (v != 2) {
    setverdict(fail, "#3: ", v);
  }

  deactivate(v_def2);

  p.send(1);
  p.receive(2); // altsteps will handle as 2 is not sent
  if (v != 1) {
    setverdict(fail, "#4: ", v);
  }

  activate(v_def2); // doesn't do anything, since v_def2 is not suspended

  p.send(1);
  p.receive(2); //altsteps will handle as 2 is not sent
  if (v != 1) {
    setverdict(fail, "#5: ", v);
  }

  var default v_def3;
  @try {
    activate(v_def3);
    setverdict(fail, "#6: error expected");
  }
  @catch (msg) {
    var template charstring exp := pattern "*Performing an activate operation on an unbound default reference.";
    if (not match(msg, exp)) {
      setverdict(fail, "#7: ", msg);
    }
  }
  
  var boolean v_true := true;
  deactivate(v_def1, v_true);
  v_def3 := activate(v_def1);
  if (v_def3 != v_def1) {
    setverdict(fail, "#8: ", v_def3, ", ", v_def1);
  }
  setverdict(pass);
}

control {
  execute(defaultIsvalue());
  execute(tc_special_default_value_unbound());
  execute(tc_special_default_value_null());
  execute(tc_special_default_value_isvalue());
  execute(tc_special_compref_unbound());
  execute(tc_special_compref_isvalue());
  execute(tc_special_default_value());
  execute(tc_special_compref());
  execute(tc_default_activate_deactivate());
}
}
