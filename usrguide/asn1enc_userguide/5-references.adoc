= References

[[_1]]
* [1]  link:https://gitlab.eclipse.org/eclipse/titan/titan.core/blob/master/usrguide/referenceguide/[Programmers Technical Reference for TITAN TTCN-3 Test Executor]

[[_2]]
* [2]  link:https://www.etsi.org/deliver/etsi_es/201800_201899/20187307/04.10.01_60/es_20187307v041001p.pdf[Methods for Testing and Specification (MTS); The Testing and Test Control Notation version 3. Part 7: Using ASN.1 with TTCN-3; European Telecommunications Standards Institute. ES 201 873-7 Version 4.10.1, April 2022]

[[_3]]
* [3]  link:https://gitlab.eclipse.org/eclipse/titan/titan.core/tree/master/usrguide/apiguide[API Technical Reference for TITAN TTCN-3 Test Executor]
